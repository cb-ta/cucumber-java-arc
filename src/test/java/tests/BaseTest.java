package tests;

import io.cucumber.java.Before;
import org.apache.commons.lang3.RandomStringUtils;
import org.openqa.selenium.WebDriver;
import utils.TestManager;

import java.io.IOException;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class BaseTest {
    protected WebDriver driver;
    protected Logger logger;

    public BaseTest()  {
        driver = TestManager.getInstance().getDriver();
        logger = TestManager.getInstance().getLogger();
    }


    public String getRandomString(String prefix, String postfix, int randomDataLength) {
        return
                prefix +
                        "_" +
                        RandomStringUtils.randomAlphanumeric(randomDataLength) +
                        "_" +
                        postfix;
    }

    public String getRandomIntegerString(int randomDataLength) {
        return RandomStringUtils.randomNumeric(randomDataLength);
    }
}
