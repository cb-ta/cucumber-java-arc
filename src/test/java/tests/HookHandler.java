package tests;

import io.cucumber.java.*;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import utils.TestManager;

import java.io.File;

public class HookHandler {
    @BeforeAll
    public static void beforeTest() {
        System.out.println("Before all");
        TestManager.getInstance();
    }

    @AfterAll
    public static void afterTest() {
        System.out.println("After all");
    }


    @Before
    public void beforeScenario() {
        System.out.println("Before");
        ChromeOptions options = new ChromeOptions();
        String path = System.getProperty("user.dir")
                + "/extensions/Adblock.crx";
        options.addExtensions(new File(path));

        WebDriver newDriver = new ChromeDriver(options);
        TestManager.getInstance().setDriver(newDriver);
    }

    @After
    public void afterScenario() {
        System.out.println("After");
        TestManager.getInstance().getDriver().quit();
        TestManager.getInstance().setDriver(null);
    }

    @BeforeStep
    public void beforeStep() {
        System.out.println("Before step");
    }

    @AfterStep
    public void afterStep() {
        System.out.println("After step");
    }
}
