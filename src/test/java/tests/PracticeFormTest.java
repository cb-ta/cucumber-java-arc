package tests;

import io.cucumber.java.en.*;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import pages.PracticeFormPage;

public class PracticeFormTest extends BaseTest {
    private PracticeFormPage practiceFormPage;
    private String randomFirstName;
    private String randomLastName;

    public PracticeFormTest() {
        practiceFormPage = new PracticeFormPage();
    }

    @Given("I am on the Practice Form Page")
    public void i_am_on_the_practice_form_page() {
        logger.info("Beginning test");
        practiceFormPage.navigate();
    }

    @When("I enter first name")
    public void i_enter_first_name() {
        randomFirstName = getRandomString("Random", "FirstName", 10);
        WebElement firstName = practiceFormPage.getFirstName();
        firstName.sendKeys(randomFirstName);
    }

    @And("I enter last name")
    public void i_enter_last_name() {
        randomLastName = getRandomString("Random", "LastName", 10);
        WebElement lastName = practiceFormPage.getLastName();
        lastName.sendKeys(randomLastName);
    }

    @Then("I should see the data in input fields")
    public void i_should_see_the_data_in_input_fields() {
        logger.info("Testing first name with: " + randomFirstName);
        WebElement firstName = practiceFormPage.getFirstName();
        Assert.assertEquals(firstName.getAttribute("value"), randomFirstName);

        logger.info("Testing last name with: " + randomLastName);
        WebElement lastName = practiceFormPage.getLastName();
        Assert.assertEquals(lastName.getAttribute("value"), randomLastName);
    }

    @But("EMail input should be empty")
    public void e_mail_input_should_be_empty() {
        logger.info("Testing email with: empty data");
        WebElement email = practiceFormPage.getEmail();
        Assert.assertEquals(email.getAttribute("value"), "");
    }
}
