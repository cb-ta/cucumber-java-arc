package pages;

import lombok.Getter;
import org.openqa.selenium.WebElement;

import org.openqa.selenium.support.FindBy;
import utils.TestManager;

@Getter
public class PracticeFormPage extends BasePage {

    @FindBy(xpath="//input[@id='firstName']")
    private WebElement firstName;
    @FindBy(xpath="//input[@id='lastName']")
    private WebElement lastName;
    @FindBy(xpath="//input[@id='userEmail']")
    private WebElement email;
    @FindBy(xpath="//label[@for='gender-radio-1']")
    private WebElement radioButtonMaleLabel;
    @FindBy(xpath="//input[@id='gender-radio-1']")
    private WebElement radioButtonMale;
    @FindBy(xpath="//input[@id='gender-radio-2']")
    private WebElement radioButtonFemale;
    @FindBy(xpath="//input[@id='gender-radio-3']")
    private WebElement radioButtonOther;
    @FindBy(xpath="//input[@id='userNumber']")
    private WebElement userNumber;
    @FindBy(xpath="//input[@id='dateOfBirthInput']")
    private WebElement dateOfBirth;
    @FindBy(xpath="//label[@for='hobbies-checkbox-1']")
    private WebElement sportsCheckBoxLabel;
    @FindBy(xpath="//label[@for='hobbies-checkbox-2']")
    private WebElement readingCheckBoxLabel;
    @FindBy(xpath="//label[@for='hobbies-checkbox-3']")
    private WebElement musicCheckBoxLabel;
    @FindBy(xpath="//input[@id='hobbies-checkbox-1']")
    private WebElement sportsCheckBox;
    @FindBy(xpath="//input[@id='hobbies-checkbox-2']")
    private WebElement readingCheckBox;
    @FindBy(xpath="//input[@id='hobbies-checkbox-3']")
    private WebElement musicCheckBox;
    @FindBy(xpath="//textarea[@id='currentAddress']")
    private WebElement currentAddress;

    public void navigate() {
        driver.get(TestManager.
                getInstance().
                getProperty("practiceFormPageUrl"));
    }

    public void navigateToNextPage() {
        driver.get(TestManager.
                getInstance().
                getProperty("dynamicPropertiesPageUrl"));
    }
}
