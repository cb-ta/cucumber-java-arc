package pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;
import utils.TestManager;

import java.util.Properties;

public class BasePage {
    protected WebDriver driver;

    public BasePage() {
        this.driver = TestManager.getInstance().getDriver();
        PageFactory.initElements(driver, this);
    }
}
