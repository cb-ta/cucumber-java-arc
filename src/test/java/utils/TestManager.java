package utils;

import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.FileHandler;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class TestManager {
    private final static String LOG_FILE_NAME = "testLogs.log";
    private final static String PROPERTY_FILE_NAME = "test.properties";

    private static TestManager instance;

    public static synchronized TestManager getInstance() {
        if (instance == null) {
            instance = new TestManager();
        }
        return instance;
    }

    private TestManager() {
        String rootPath = Thread.
                currentThread().
                getContextClassLoader().
                getResource("").
                getPath();

        try {
            logger = Logger.getLogger("TestLogs");
            String logFilePath = rootPath + LOG_FILE_NAME;
            FileHandler fh = new FileHandler(logFilePath);
            logger.addHandler(fh);
            SimpleFormatter formatter = new SimpleFormatter();
            fh.setFormatter(formatter);

            String propFilePath = rootPath + PROPERTY_FILE_NAME;
            properties = new Properties();
            properties.load(new FileInputStream(propFilePath));
        } catch (IOException exc) {
            System.out.println(exc.getMessage());
        }
    }

    private WebDriver driver;
    private Properties properties;
    private Logger logger;

    public WebDriver getDriver() {
        return driver;
    }

    public void setDriver(WebDriver driver) {
        this.driver = driver;
    }

    public Logger getLogger() {
        return logger;
    }

    public String getProperty(String key) {
        return properties.getProperty(key, "");
    }
}
