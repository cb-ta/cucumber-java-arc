Feature:  Practice Form
  As a customer I want to fill out practice form
  so that I can submit it.

  Scenario: Visually validate first and last name
    Given I am on the Practice Form Page
    When I enter first name
    And I enter last name
    Then I should see the data in input fields
    But EMail input should be empty

  Scenario: Visually validate first and last name 2
    Given I am on the Practice Form Page
    When I enter first name
    And I enter last name
    Then I should see the data in input fields
    But EMail input should be empty